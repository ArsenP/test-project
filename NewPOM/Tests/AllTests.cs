﻿using Shufersal.Pages;
using NUnit.Framework;
using Shufersal;
using Assert = NUnit.Framework.Assert;

namespace Shufersal
{
    class Test : BaseClass
    {



        static void Main(string[] args)
        {
        }

        LoginPage loginPage;
        HomePage homePage;
        ProductsPage productPage;


        [OneTimeSetUp]
        public void Setup()
        {
            BaseClass.DriverInitialization("ch");
            loginPage = new LoginPage();
            homePage = new HomePage();
            productPage = new ProductsPage();
        }

        [Test, Order(1)]
        public void LoginAsValidUser()
        {
            loginPage.ClickOnLoginLink();
            loginPage.EnterUserName("audit1551@gmail.com");
            loginPage.EnterUserPassword("asterixodin");
            loginPage.ClickOnLoginButton();
        }

        [Test, Order(2)]
        public void ValidateLoggedUser()
        {
            Assert.AreEqual(loginPage.VerifyUser(), "שלום ארסן");
        }

        [Test, Order(3)]
        public void EnterTextInSearchField()
        {
            homePage.EnterTextInSearchField();
        }

        [Test, Order(4)]
        public void ClickOnLink()
        {
            homePage.ClickOnLink();
        }

        [Test, Order(5)]
        public void CliickOnViewListButton()
        {
            productPage.CliickOnViewListButton();
        }

        [Test, Order(6)]
        public void PrintElements()
        {
            productPage.PrintElements();
        }


        [OneTimeTearDown]
        public void Close()
        {
            //driver.Quit();
        }
    }
}
