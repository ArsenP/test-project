﻿using Shufersal;
using OpenQA.Selenium;
using System;


namespace Shufersal
{
    class ProductsPage : BaseClass
    {

        public void CliickOnViewListButton()
        {
            WaitForElementIsPresent(PageElements.ListViewButton).Click();
        }

        public void PrintElements ()
        {
            string str;
            foreach (IWebElement element in PageElements.Prices)
            {
                str = element.Text;
                Console.WriteLine(str);
            }
        }
    }
}
