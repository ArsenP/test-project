﻿using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace Shufersal
{
    class PageElements : BaseClass
    {
        // Loogin Page
        public static IWebElement LoginLink => driver.FindElement(By.Id("loginDropdownContainer"));
        public static IWebElement UserName => driver.FindElement(By.Id("j_username"));
        public static IWebElement UserPassword => driver.FindElement(By.Id("j_password"));
        public static IWebElement LoginButton => driver.FindElement(By.XPath("//div[@class='bottomSide']/button"));
        public static IWebElement LoggedUser => driver.FindElement(By.XPath("//a[@class='info']"));

        //Home Page

        public static IWebElement SearchField => driver.FindElement(By.Id("js-site-search-input"));
        public static IWebElement ProductLink => driver.FindElement(By.XPath("//button[@class='js_search_button allProductsTitle']"));

        // Products List

        public static IWebElement ListViewButton => driver.FindElement(By.XPath("//div[@class='chooseFilter']//li[@class='listView']/button"));

        public static IList<IWebElement> Prices => driver.FindElements(By.XPath("//div[@class='line']//span[@class='price']//span[@class='number']"));
                                                                           

    }
}
