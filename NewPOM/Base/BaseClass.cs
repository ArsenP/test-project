﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using WebDriverManager.DriverConfigs.Impl;

namespace Shufersal
{
    class BaseClass
    {

        public static IWebDriver driver;
        public static string URL = "https://www.shufersal.co.il/";

        public static void DriverInitialization(string browser)
        {
            if (driver == null)
            {
                if (browser.ToLower().Equals("ch"))
                {
                    new WebDriverManager.DriverManager().SetUpDriver(new ChromeConfig());
                    driver = new ChromeDriver();
                    ManageDriver();
                }
                else if (browser.ToLower().Equals("ff"))
                {
                    new WebDriverManager.DriverManager().SetUpDriver(new FirefoxConfig());
                    driver = new FirefoxDriver();
                    ManageDriver();
                }
                else if (browser.ToLower().Equals("ed"))
                {
                    new WebDriverManager.DriverManager().SetUpDriver(new EdgeConfig());
                    driver = new EdgeDriver();
                    ManageDriver();
                }
                else
                {
                    driver.Quit();
                }
            }
        }

        private static void ManageDriver()
        {
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(URL);
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
        }

        //public IWebElement WaitForElementIsPresent(IWebElement element)
        //{
        //        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
        //        return wait.Until(ExpectedConditions.ElementToBeClickable(element));
        //}

        //public IWebElement ElementIsPresent(IWebElement element)
        //{
        //    WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
        //    return wait.Until(ExpectedConditions.ElementIsVisible((By)element));

        //}

        public IWebElement WaitForElementIsPresent(IWebElement element)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
        }
    }
}
