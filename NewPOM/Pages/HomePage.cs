﻿using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shufersal.Pages
{
    class HomePage : BaseClass
    {

        public void EnterTextInSearchField()
        {
            WaitForElementIsPresent(PageElements.SearchField).SendKeys("חלב");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
        }

        public void ClickOnLink()
        {
                WaitForElementIsPresent(PageElements.ProductLink).Click();
        }
    }
}
