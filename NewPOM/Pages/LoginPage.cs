﻿using OpenQA.Selenium;
using System;

namespace Shufersal
{
    class LoginPage : BaseClass
    {

        public void ClickOnLoginLink()
        {
            WaitForElementIsPresent(PageElements.LoginLink).Click();
        }

        public void EnterUserName(string username)
        {
            WaitForElementIsPresent(PageElements.UserName).SendKeys(username);
        }

        public void EnterUserPassword(string password)
        {
            WaitForElementIsPresent(PageElements.UserPassword).SendKeys(password);
        }

        public void ClickOnLoginButton()
        {
            WaitForElementIsPresent(PageElements.LoginButton).Click();
        }

        public string VerifyUser()
        {
            string user = null; ;
            try
            {
                user = WaitForElementIsPresent(PageElements.LoggedUser).Text.Trim();
            } catch (NoSuchElementException e)
            {
                Console.Write(e);
                
            } finally
            {
                user = WaitForElementIsPresent(PageElements.LoggedUser).Text.Trim();
            }
            return user;
        }
    }
}
